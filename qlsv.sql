--5.1
CREATE TABLE DMKHOA (
    MaKH			Varchar(6)		PRIMARY KEY,
    TenKhoa			Varchar(30)		NOT NULL
    );
    
CREATE TABLE SINHVIEN (
    MaSV			Varchar(6)		PRIMARY KEY,
    HoSV			Varchar(30)		NOT NULL,
    TenSV			Varchar(15)		NOT NULL,
    GioiTinh 		Char(1)			NOT NULL,
    NgaySinh		DateTime,
    NoiSinh			Varchar(50)		NOT NULL,
    MaKH			Varchar(6)		NOT NULL,
    HocBong			Int 
    );

ALTER TABLE 	SINHVIEN 
ADD CONSTRAINT 	fk_makh
FOREIGN KEY 	(MaKH)
REFERENCES 	DmKHOA(MaKH);
